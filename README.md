# Contoh Telegram BOT
username bot: rosalina12_bot

Token Bot: 676069328:AAH5xCZ6YBMTIFMxwdeBDzLOtNyHY7WW1wU

## Mengambil Identitas BOT
Untuk Mengambil Identitas Dari Bot https://api.telegram.org/bot<bot_token>/getMe
lalu masukan di URL Browser sebagai Contoh
``` bash
https://api.telegram.org/bot676069328:AAH5xCZ6YBMTIFMxwdeBDzLOtNyHY7WW1wU/getMe
```

## Mengambil Data Chat Terakhir
Untuk Mengambil Data Chat Terakhir Ganti Function Get Me Jadi url/getUpdates
Contoh:
``` bash
https://api.telegram.org/bot676069328:AAH5xCZ6YBMTIFMxwdeBDzLOtNyHY7WW1wU/getUpdates
```
Response:
example data response
``` json
{
	"ok": true,
	"result": [{
		"update_id": 848917096,
		"message": {
			"message_id": 1,
			"from": {
				"id": 585336030, #ini adalah id target
				"is_bot": false,
				"first_name": "Iank",
				"username": "iank5",
				"language_code": "en-US"
			},
			"chat": {
				"id": 585336030,
				"first_name": "Iank",
				"username": "iank5",
				"type": "private"
			},
			"date": 1536589620,
			"text": "/start",
			"entities": [{
				"offset": 0,
				"length": 6,
				"type": "bot_command"
			}]
		}
	}]
}
```

## Mengirim Chat
Untuk Mengirim Chat Ocha Harus Tau id telegram dari target
Urlnya Seperti ini https://api.telegram.org/bot<bot_token>/sendMessage?chat_id=<id_target>&text="<Pesan Harus ditutup dengan tanda kutip>"


example:
``` bash
https://api.telegram.org/bot676069328:AAH5xCZ6YBMTIFMxwdeBDzLOtNyHY7WW1wU/sendMessage?chat_id=676069328&text="Hello from SMTFirstBot"
```
contoh response:
``` bash
{
	"ok": true,
	"result": {
		"message_id": 3,
		"from": {
			"id": 676069328,
			"is_bot": true,
			"first_name": "OchaBot",
			"username": "rosalina12_bot"
		},
		"chat": {
			"id": 585336030,
			"first_name": "Iank",
			"username": "iank5",
			"type": "private"
		},
		"date": 1536591382,
		"text": "\"Hello from SMTFirstBot\""
	}
}
```

tips untuk mengambil chat_id target
1. pada telegram target cari username rosalina12_bot kemudian start percakapan dengan bot tersebut
2. pada telegram target ketik /start kemudian kirim
3. pada bot kita masukan link mengambil data chat Terakhir diatas (liat mengambil data terakhir chat)


## Tips
agar semua tindakan diatas bisa realtime ocha harus eksekusi file ini via cli ph
example
``` bash
php nama_file_php.php
```
agar bisa memanfaatkan ini ocha mesti path php nya dan php-cli harus diinstal (kalo pake xampp biasanya sudah include)

## Run Example
Jika ocha yakin sudah nge path phpnya dan install php-cli
di cmd jalankan perintah berikut ini:
``` bash
php telegram.php
```
harus koneksi internet
